
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import { TextField } from "@mui/material";
import { Button } from "@mui/material";

const UpdateTask = () => {
    const navigate = useNavigate();
    const { title } = useParams();
    const [task, setTask] = useState([]);
    const [error, setError] = useState("")
    const [info, setInfo] = useState("")
    const [updateTask, setUpdateTask] = useState({ title: '', description: '', deadline: '', status: 'to_do' });

    useEffect(() => {
        const task = JSON.parse(localStorage.getItem("task"));
        if (task) {
            setTask(task);
            const curTask = task.filter((task) => task.title === title);
            setUpdateTask(curTask[0]);

        } else {
            setTask([]);
            setError("Task not found");
        }

    } , []);

    const handleChange = (event, field) => {
        const newData = { ...updateTask };
        newData[field] = event.target.value;
        setUpdateTask(newData);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setInfo("");
        if (updateTask.title === "" || updateTask.description === "" || updateTask.deadline === "") {
            setError("Please fill all fields")
        }else{
            setError("")
            //callback to submit new task
            const newTaskList = task.filter((task) => task.title !== title);
            newTaskList.push(updateTask);
            setTask(newTaskList);
            localStorage.setItem("task", JSON.stringify(newTaskList));
            setInfo("Task updated");
            navigate("/task");
        }
    }

    if(task){
        return (
            <div>
                <h1>Update task</h1>
                <p style={{color: "red"}} >{error}</p>
                <p style={{color: "green"}}>{info}</p>
                <form onSubmit={handleSubmit} id="updateFormTask">

                    <TextField id="standard-basic" label="Title" variant="standard" name="title" type="text" value={updateTask.title} onChange={(e) => handleChange(e,'title')} />
                    <br/>

                    <TextField id="standard-multiline-basic" label="Description" variant="standard" name="description" type="text" maxRows={4} multiline value={updateTask.description} onChange={(e) => handleChange(e,'description')} />
                    <br/>
                    <label htmlFor="deadline">Deadline</label>
                    <input
                        type="datetime-local"
                        name="deadline"
                        value={updateTask.deadline}
                        onChange={(e) => handleChange(e,'deadline')}
                    />
                    <br/>
                    <label htmlFor="status">Status</label>
                    <select
                        name="status"
                        value={updateTask.status}
                        onChange={(e) => handleChange(e,'status')}
                    >
                        <option value="to_do">To do</option>
                        <option value="in_progress">In progress</option>
                        <option value="done">Done</option>
                    </select>
                    <br/>
                </form>
                <Button variant="contained" color="success" form="updateFormTask" type="submit">Update task</Button>
                <Button variant="contained" color="error" onClick={() => (navigate('/dashboard'))}>Cancel</Button>
            </div>
        );
    
    }else{
        return (
            <div>
                <h1>Update task</h1>
                <p style={{color: "red"}} >{error}</p>
                <Button variant="contained" color="error" onClick={() => (navigate('/dashboard'))}>Cancel</Button>
            </div>
        );
    }

}

export default UpdateTask;