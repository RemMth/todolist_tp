import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";

import { TextField } from "@mui/material";
import { Button } from "@mui/material";

const NewTask = () => {
    const navigate = useNavigate();
    const [newTask, setNewTask] = useState({ title: '', description: '', deadline: '', status: 'to_do' });
    const [task, setTask] = useState([]);
    const [error, setError] = useState("")
    const [info, setInfo] = useState("")

    useEffect(() => {
        const task = JSON.parse(localStorage.getItem("task"));
        if (task) {
            setTask(task);
        } else {
            setTask([]);
        }
    }, []);


    const handleChange = (event, field) => {
        const newData = { ...newTask };
        newData[field] = event.target.value;
        setNewTask(newData);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setInfo("");
        if (newTask.title === "" || newTask.description === "" || newTask.deadline === "") {
            setError("Please fill all fields")
        }else{
            setError("")
            //callback to submit new task
            const newTaskList = [...task, newTask];
            setTask(newTaskList);
            localStorage.setItem("task", JSON.stringify(newTaskList));
            setNewTask({ title: '', description: '', deadline: '', status: 'to_do' });
            setInfo("Task added");
        }
    };

    return (
        <div>
            <h1>Add new task</h1>
            <p style={{color: "red"}} >{error}</p>
            <p style={{color: "green"}}>{info}</p>
            <form onSubmit={handleSubmit} id="createTaskForm">

                <TextField id="standard-basic" label="Title" variant="standard" name="title" type="text" value={newTask.title} onChange={(e) => handleChange(e,'title')} />
                <br/>

                <TextField id="standard-multiline-basic" label="Description" variant="standard" name="description" type="text" maxRows={4} multiline value={newTask.description} onChange={(e) => handleChange(e,'description')} />
                <br/>
                <label htmlFor="status">Deadline</label>
                <br />
                <input
                    type="datetime-local"
                    name="deadline"
                    value={newTask.deadline}
                    onChange={(e) => handleChange(e,'deadline')}
                />
                <br/>

                <label htmlFor="status">Status</label>
                <br/>
                <select name="status" value={newTask.status} onChange={(e) => handleChange(e,'status')}>
                    <option value="to_do">To_do</option>
                    <option value="done">Done</option>
                </select>
                <br/>
            </form>
            <Button variant="contained" color="success" form="createTaskForm" type="submit">Add task</Button>
            <Button variant="contained" color="error" onClick={() => (navigate('/dashboard'))}>Cancel</Button>

        </div>
    );
}

export default NewTask;


