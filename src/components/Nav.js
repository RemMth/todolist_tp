import { Outlet, useNavigate } from "react-router-dom";
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

const Nav = () => {

  //redirect to /dashboard
  const navigate = useNavigate();

  React.useEffect(() => {
    //si la route est /
    if (window.location.pathname === "/") {
      navigate("/dashboard");
    }
  }, [navigate]);

  
  return (
    <>
      <AppBar position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            <Button key="dashboard" sx={{ my: 2, color: 'white', display: 'block' }} onClick={() => {navigate('dashboard')}} > Dashboard </Button>
            <Button key="task" sx={{ my: 2, color: 'white', display: 'block' }} onClick={() => {navigate('task')}} > Search </Button>
            <Button key="newtask" sx={{ my: 2, color: 'white', display: 'block' }} onClick={() => {navigate('newtask')}} > New Task </Button>

            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Outlet />
    </>
    
  )
};

export default Nav;