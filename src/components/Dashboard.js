
import { useEffect } from "react";
import { useState } from "react";
import {useNavigate} from "react-router-dom";

import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid } from "@mui/material";


const Dashboard = () => {
    const navigate = useNavigate();
    const [task, setTask] = useState([]);
    const [done, setDone] = useState(0);
    const [notDone, setNotDone] = useState(0);
    const [late, setLate] = useState(0);
    const [total, setTotal] = useState(0);
    const [lateTask, setLateTask] = useState([]);

    //function delete
    const deleteTask = (selectedTask) => {
        const newTask = task.filter((task) => task !== selectedTask);
        setTask(newTask);
        localStorage.setItem("task", JSON.stringify(newTask));
    };


    //function update makeDone
    const makeDone = (selectedTask) => {
        const newTask = task.map((task) => {
            if (task === selectedTask) {
                task.status = "done";
            }
            return task;
        }
        );
        setTask(newTask);
        localStorage.setItem("task", JSON.stringify(newTask));
    };


    useEffect(() => {
        const task = JSON.parse(localStorage.getItem("task"));
        if (task) {
            setTask(task.sort((a, b) => new Date(a.deadline) - new Date(b.deadline)));

            setTask(task);
            setTotal(task.length);
            const doneTask = task.filter((task) => task.status === "done");
            setDone(doneTask.length);
            const notDoneTask = task.filter((task) => task.status === "to_do");
            setNotDone(notDoneTask.length);
            const lateTask = task.filter((task) => (new Date(task.deadline) < new Date() && task.status === "to_do"));
            setLate(lateTask.length);
            setLateTask(lateTask);
        } else {
            setTask([]);
        }
    }, []);





    return (
        <div>
            <h1>Dashboard</h1>
    
            <Grid container spacing={2}>
                {task.map((task) => (
                    <><Grid item xs={3}>
                    <Card>
                        <CardContent>
                            <Typography variant="h5" component="div">
                                {task.title}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {task.description}
                            </Typography>
                            <Typography variant="body2">
                            {task.status}
                                <br />
                                {(new Date(task.deadline) < new Date() && task.status === "to_do") ? <p style={{ color: "red" }}>{task.deadline}</p> : <p>{task.deadline}</p>}

                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ButtonGroup variant="contained" aria-label="outlined primary button group">
                                    {task.status != "done" ? <Button variant="contained" color="success" onClick={() => makeDone(task)}>Done</Button> : null}
                                    <Button variant="contained" color="error" onClick={() => deleteTask(task)}>Delete</Button>
                                    <Button variant="contained" onClick={() => (navigate('/updateTask/' + task.title))}>Edit</Button>
                            
                            </ButtonGroup>
                        </CardActions>
                    </Card>
                    </Grid></>
                ))}
            </Grid>
            <h2>Stats</h2>
            <p>Done: {done} ({Math.round((done / total) * 100)}%)</p>
            <p>To Do: {notDone} ({Math.round((notDone / total) * 100)}%)</p>
            <p>Late: {late} ({Math.round((late / total) * 100)}%)</p>
            {lateTask.length > 0 ? <h2>List of late tasks</h2> : null}
            <ul>
                {lateTask.map((task) => (
                    <li key={task.title}>{task.title}</li>
                ))}
            </ul>

        </div>

    );
};



export default Dashboard;

