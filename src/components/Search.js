import {useState} from "react";
import { TextField } from "@mui/material";
export const Search = (props) => {

    const {initialSearch, onSearchChange} = props;
    const [search, setSearch] = useState(initialSearch);

    const changeSearch = (event) => {
        onSearchChange(event.target.value);
        setSearch(event.target.value);
    }

    return (
        <TextField id="standard-basic" label="Search" variant="standard" type="text" value={search} onChange={changeSearch} />
    );
}
