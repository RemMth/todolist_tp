import { useEffect } from "react";
import { useState } from "react";
import {useNavigate} from 'react-router-dom';

import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid } from "@mui/material";
import { TextField } from "@mui/material";

//render task that are in localStorage and create a searchbar to search for task
const Task = () => {
    const navigate = useNavigate();
    const [task, setTask] = useState([]);
    const [search, setSearch] = useState("");
    const [filteredTask, setFilteredTask] = useState([]);

    useEffect(() => {
        const task = JSON.parse(localStorage.getItem("task"));
        if (task) {
            setTask(task.sort((a, b) => new Date(a.deadline) - new Date(b.deadline)));
        } else {
            setTask([]);
        }
        setTask(task);
    }, []);


    //function delete
    const deleteTask = (selectedTask) => {
        const newTask = task.filter((task) => task !== selectedTask);
        setTask(newTask);
        localStorage.setItem("task", JSON.stringify(newTask));
    };


    //function update makeDone
    const makeDone = (selectedTask) => {
        const newTask = task.map((task) => {
            if (task === selectedTask) {
                task.status = "done";
            }
            return task;
        }
        );
        setTask(newTask);
        localStorage.setItem("task", JSON.stringify(newTask));
    };



    useEffect(() => {
        if (search == "") {
            setFilteredTask(task);
        }else{
            setFilteredTask(
                task.filter((task) =>
                    task.title.toLowerCase().includes(search.toLowerCase())
                )
            );
        }
    }, [search, task]);

    return (
        <div>
            <h1>Search Task</h1>
            <TextField id="standard-basic" label="Search" variant="standard" type="text" value={search} onChange={(e) => setSearch(e.target.value)} />

            <Grid container spacing={2}>
                {filteredTask.map((task) => (
                    <><Grid item xs={3}>
                    <Card>
                        <CardContent>
                            <Typography variant="h5" component="div">
                                {task.title}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {task.description}
                            </Typography>
                            <Typography variant="body2">
                            {task.status}
                                <br />
                                {(new Date(task.deadline) < new Date() && task.status === "to_do") ? <p style={{ color: "red" }}>{task.deadline}</p> : <p>{task.deadline}</p>}

                            </Typography>
                        </CardContent>
                        <CardActions>
                        <ButtonGroup variant="contained" aria-label="outlined primary button group">
                                {task.status != "done" ? <Button variant="contained" color="success" onClick={() => makeDone(task)}>Done</Button> : null}
                                <Button variant="contained" color="error" onClick={() => deleteTask(task)}>Delete</Button>
                                <Button variant="contained" onClick={() => (navigate('/updateTask/' + task.title))}>Edit</Button>
                            </ButtonGroup>
                        </CardActions>
                    </Card>
                    </Grid></>
                ))}
            </Grid>

            
            
        </div>
    );
}

export default Task;
