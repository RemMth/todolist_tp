import { BrowserRouter, Routes, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import Task from "./components/Task"
import Nav from "./components/Nav";
import NewTask from "./components/task/addTask";
import UpdateTask from "./components/task/updateTask";
import React from "react";



export default function App() {
    return (

        <BrowserRouter>
            <Routes>
                    <Route path="/" element={<Nav />}  >
                    <Route path="/Dashboard" element={<Dashboard/>}/>
                    <Route path="task" element={<Task />} />
                    <Route path="newTask" element={<NewTask />} />
                    <Route path="updateTask/:title" element={<UpdateTask />} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

