# TP TodoList 

## Dashboard

![alt text](https://gitlab.com/RemMth/todolist_tp/-/raw/main/README_IMG/dashboard.png)

## Search List

![alt text](https://gitlab.com/RemMth/todolist_tp/-/raw/main/README_IMG/searchList.png)

## New Task

![alt text](https://gitlab.com/RemMth/todolist_tp/-/raw/main/README_IMG/newTask.png)

## Update Task

![alt text](https://gitlab.com/RemMth/todolist_tp/-/raw/main/README_IMG/updateTask.png)
